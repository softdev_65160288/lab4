/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author informatics
 */
public class Table {
    private String [][] table = {{"1","2","3"},{"4","5","6"},{"7","8","9"}};
    private Player Player1;
    private Player Player2;
    private Player currentPlayer;
    
    public Table(Player Player1,Player Player2){
        this.Player1 = Player1;
        this.Player2 = Player2;
        this.currentPlayer = Player1;
    }
    public String[][] getTable() {
        return table;
    } 
    public Player getCurrentPlayer(){
        return currentPlayer;
    }
    public void setTable(String num){
        if(num.equals("1")){
                table[0][0] = currentPlayer.getSymbol();
            } else if (num.equals("2")) {
                table[0][1]=currentPlayer.getSymbol();
            } else if (num.equals("3")) {
                table[0][2]=currentPlayer.getSymbol();
            } else if (num.equals("4")) {
                table[1][0]=currentPlayer.getSymbol();
            } else if (num.equals("5")) {
                table[1][1]=currentPlayer.getSymbol();
            } else if (num.equals("6")) {
                table[1][2]=currentPlayer.getSymbol();
            } else if (num.equals("7")) {
                table[2][0]=currentPlayer.getSymbol();
            } else if (num.equals("8")) {
                table[2][1]=currentPlayer.getSymbol();
            } else if (num.equals("9")) {
                table[2][2]=currentPlayer.getSymbol();
            }
    }
    
    public void switchPlayer(){
        if(currentPlayer==Player1){
            currentPlayer = Player2;
        }else{
            currentPlayer = Player1;
        }
    }
    public void isWin(){
       System.out.println(currentPlayer.getSymbol() + " !!!Winner!!!"); 
    }
    
    public void isDraw(){
       System.out.println(" !!!Draw!!!"); 
    }
    public boolean checkWin(){
        if((checkRow() || checkCol() || checkDia())){
            return true;
        }else{
            return false;
        }
    }
    public boolean checkDraw(){
        if (table[0][0].equals("1") || table[0][1].equals("2")  || table[0][2].equals("3")  || table[1][0].equals("4")  || table[1][1].equals("5")  || table[1][2].equals("6")  || table[2][0].equals("7")  || table[2][1].equals("8")  || table[2][2].equals("9")) {
            return false;
        }
        return true;
    }
    public boolean checkRow(){
        for(int i=0;i<3;i++){
            if(table[0][0].equals(currentPlayer.getSymbol()) &&table[0][1].equals(currentPlayer.getSymbol()) && table[0][2].equals(currentPlayer.getSymbol())){
                return true;
            }else if(table[1][0].equals(currentPlayer.getSymbol()) && table[1][1].equals(currentPlayer.getSymbol()) && table[1][2].equals(currentPlayer.getSymbol())){
                return true;
            }else if(table[2][0].equals(currentPlayer.getSymbol()) && table[2][1].equals(currentPlayer.getSymbol()) && table[2][2].equals(currentPlayer.getSymbol())){
                return true;
            }
        }
        return false;
    }
    public boolean checkCol(){
        for(int i=0;i<3;i++){
            if(table[0][0].equals(currentPlayer.getSymbol()) && table[1][0].equals(currentPlayer.getSymbol())&&table[2][0].equals(currentPlayer.getSymbol())){
                return true;
            }else if(table[0][1].equals(currentPlayer.getSymbol())&& table[1][1].equals(currentPlayer.getSymbol())&&table[2][1].equals(currentPlayer.getSymbol())){
                return true;
            }else if(table[0][2].equals(currentPlayer.getSymbol()) && table[1][2].equals(currentPlayer.getSymbol())&&table[2][2].equals(currentPlayer.getSymbol())){
                return true;
            }
        }
        return false;
    }
    public boolean checkDia(){
        for(int i=0;i<3;i++){
            if(table[0][0].equals(currentPlayer.getSymbol()) && table[1][1].equals(currentPlayer.getSymbol())&&table[2][2].equals(currentPlayer.getSymbol())){
                return true;
            }else if(table[0][2].equals(currentPlayer.getSymbol()) && table[1][1].equals(currentPlayer.getSymbol())&&table[2][0].equals(currentPlayer.getSymbol())){
                return true;
            }            
        }
        return false;
    }
    
    
}

