/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author informatics
 */
public class Game {
    static Set<String> chosenNumbers = new HashSet<>();
    private Table table;
    private Player Player1,Player2;
    private boolean gameOver = false;
    static String num;
    
    public Game(){
        Player1 = new Player("X");
        Player2 = new Player("O");
        table = new Table(Player1,Player2);
    }
    public void play(){
        showWelcome();
        while(true){
            gameOver = false;
            showTable();
            showTurn();
            inputNumber();
            if(table.checkWin() == true){
                showTable();
                table.isWin();
                gameOver = true;
                saveStat();
                showStat();
            }
            if(table.checkDraw() == true){
                table.isDraw();
                gameOver = true;
                saveStat();
                showStat();
            }
            if(gameOver == true){
                if(newGame().equalsIgnoreCase("Y")){
                    resetGame();
                    showWelcome();
                    continue;
                }else{
                    break;
                }
            }
            table.switchPlayer();
        }
    }
    private void showWelcome(){
        System.out.println("Welcome to XO Game");
    }
    private void showTable(){
        String[][] t = table.getTable();
        for(int i = 0;i<3;i++){
            for(int j =0;j<3;j++){
                System.out.print(t[i][j]+" "); 
            }
            System.out.println();
        }
    }
    private String newGame(){
        System.out.println("Play agian (Y/N) :");
        Scanner kb = new Scanner(System.in);
        String playAgain = kb.next();
        return playAgain;
    }
    
    public void resetGame(){
        chosenNumbers.clear();
        table = new Table(Player1,Player2);
    }

    private void inputNumber() {
        Scanner kb = new Scanner(System.in);
        System.out.println("Please input number");
        num = kb.next();
        checkRepeat();
        table.setTable(num);
    }
    private void showTurn(){
        System.out.println(table.getCurrentPlayer().getSymbol()+" Turn");
    }
    
    private String checkRepeat(){
        while (chosenNumbers.contains(num)) {
            showTable();
            showTurn();
            inputNumber();
            }
        chosenNumbers.add(num);
        return num;
        
    }
    private void saveStat(){
        if(table.getCurrentPlayer().getSymbol() == "X"){
            Player1.win();
            Player2.lose();
        }else if(table.getCurrentPlayer().getSymbol() == "O"){
            Player2.win();
            Player1.lose();
        }else{
            Player1.draw();
            Player2.draw();
        }

    }
    private void showStat(){
        System.out.println("-------------------------------------------");
        System.out.println("X Win :"+Player1.getWinCount());
        System.out.println("X Lose :"+Player1.getLoseCount());
        System.out.println("X Draw :"+Player1.getDrawCount());
        System.out.println("O Win :"+Player2.getWinCount());
        System.out.println("O Lose :"+Player2.getLoseCount());
        System.out.println("O Draw :"+Player2.getDrawCount());
        System.out.println("-------------------------------------------");
    }
    
}
